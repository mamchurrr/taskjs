function NamesList(data) {
    
    const {
        container = '.names-list',
        inputName = '.inputName', 
        buttonInput = '.buttonInput', 
        buttonDelete = '.buttonDelete', 
        buttonDeleteAll = '.buttonDeleteAll', 
    } = data;


    this.container = document.querySelector(container);
    this.inputName = document.querySelector(inputName);
    this.buttonInput = document.querySelector(buttonInput);
    this.buttonDelete = document.querySelector(buttonDelete);
    this.buttonDeleteAll = document.querySelector(buttonDeleteAll);
    this.list = document.createElement('ul');

    this.InputData = () => {
        let  listItem = document.createElement('li');
        if(!this.inputName.value){
            alert('Input Name')
        }else {
            listItem.innerHTML = this.inputName.value;
            this.list.appendChild(listItem); 
        }
        this.container.appendChild(this.list);
        this.inputName.value = "";
    }
    this.deleteName = () => {
        if(this.list.children.length){
            let  listItem = this.list.children[0];
            listItem.remove();   
        }
    }
    this.deleteAll = () => {
        let arrayList = Array.from(this.list.children);
        arrayList.forEach(element => {
            element.remove()
            });    
    }

    this.buttonInput.onclick = this.InputData;    
    this.inputName.addEventListener('keydown', (e) => {
        if (e.keyCode === 13) {
            this.InputData()
        }else if (e.keyCode === 46){
            this.deleteName()
        }
    })    
    this.buttonDelete.onclick = this.deleteName;
    this.buttonDeleteAll.onclick = this.deleteAll;
}



new NamesList({
    container: '.names-list',
});