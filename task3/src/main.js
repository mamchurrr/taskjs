import {colors} from "./data.js"; 

function ColorSlider(data) {
    const {
        container_id = '.square', 
        colors = ['red', 'blue', 'green', 'yellow'], 
        defaultColor = 0,        
    } = data;

    this.container_id = container_id;
    this.colors = colors;
    this.activeColor = defaultColor;

    this.container = document.querySelector(container_id);
    this.wrapper = document.createElement('div')
    this.count = colors.length - 1;
    this.btnRight = document.querySelector('.btn-right');
    this.btnLeft = document.querySelector('.btn-left');
    
    this.changeColorRight = (e) => {
        if(this.activeColor === 0) {
            this.activeColor = this.count;
        } else {
            this.activeColor--;
        }
        this.render();
    }

    this.changeColorLeft = () => {
        if(this.activeColor === this.count) {
            this.activeColor = 0;
        } else {
            this.activeColor++;
        }
        this.render();
    }

    this.render = function () {
        this.wrapper.remove();  
        this.wrapper.style.height = "200px";
        this.wrapper.style.width = "200px";
        this.wrapper.style.border = "1px solid black";
        this.setActiveColor();
        this.container.appendChild(this.wrapper);
    }

    this.setActiveColor = () => {
        this.wrapper.style.backgroundColor = this.colors[this.activeColor];
    }

    this.btnLeft.onclick = this.changeColorLeft;
    this.btnRight.onclick = this.changeColorRight;

    this.render();
}

new ColorSlider({
    container_id: '.square',
    colors: colors,
    defaultColor: 2,
});

