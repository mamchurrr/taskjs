'use strict';

class ModelSquare {
    constructor(elSelector, upButton, rightButton, leftButton, downButton){
        this.elem = document.querySelector(elSelector)
        this.box = this.elem.getBoundingClientRect()
        this.coordinateEl = {
            top: this.box.top + pageYOffset,
            left: this.box.left + pageXOffset
        }
        this.upButton = document.querySelector(upButton)
        this.rightButton = document.querySelector(rightButton) 
        this.leftButton =  document.querySelector(leftButton)
        this.downButton =  document.querySelector(downButton)                           
    }
}
class ControllerSquare extends ModelSquare {
    constructor(elSelector, upButton,){
        super(elSelector, upButton)
    }
    
    upMove() {
         this.upButton.onclick =  () => {
            
            let start = Date.now();
            let timer = setInterval(() => {
                console.dir(this.elem.style)
                var timePassed = Date.now() - start;
                this.elem.style.top = this.coordinateEl.top - timePassed + 'px';          
                if (timePassed > 2000 || this.elem.style.top < '0px') clearInterval(timer);            
            }, 20 )
            console.log(this.coordinateEl.top)
        } 
    }
}



let controller = new ControllerSquare('.square','#up');

controller.upMove()


