
function NamesList(data) {
    
    const {
        container = '.names-list',
        names = [], 
        fraze = 'Вы нажали на имя:',
    } = data;

    this.names = names;
    this.fraze = fraze;

    this.container = document.querySelector(container);
    this.list = document.createElement('ul');

    this.render = function () {
      this.names.forEach(element => {
        let  listItem = document.createElement('li');
        listItem.innerHTML = element;
        this.list.appendChild(listItem);        
      });
            
        this.container.appendChild(this.list);
    }

    this.liAlert = (e) => {
        let name = e.target.innerHTML;
        let alertMessage = `${this.fraze} ${name}`;
        alert(alertMessage);
    }

    this.list.onclick = this.liAlert;

    this.render();
}



new NamesList({
    container: '.names-list',
    names: names, 
    fraze:'Вы нажали на имя:',
});

